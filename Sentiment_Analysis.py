
# coding: utf-8

# ### --------------------------------------------------------------         Sentiment Analysis         ------------------------------------------------------------------

# ### Steps in sentiment analysis :-
#  0.  Import required packages
#  1.  Data cleaning (NA, Special characters, lower etc.)
#  2.  Remove strong words
#  3.  Abbrevations & Spell check
#  4.  Tokenazation & Remove stop words
#  5.  Lemetization & stemming
#  6.  Synonyms  [Pending]
#  7.  Remove rare words and common words
#  8.  Countvectorizer (Chi-square feature extraction)
#  9.  Model build with features
#  10. Testing on new data
#  11. TF-IDF
#  12. Topic Model [Pending]

# #### 0. Import data & required packages

# In[ ]:

# Import required basic packages
import pandas as pd
import numpy as np
import gc
gc.collect()

# Import nltk  packages
import re
import nltk
from nltk.corpus import wordnet
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
import string
import time
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import PorterStemmer
from nltk.stem import LancasterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2,f_classif,mutual_info_classif

# model packages
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

import warnings
warnings.filterwarnings('ignore')


# In[ ]:

# Import data
train_tweet = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Text/train.csv")
test_tweet = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Text/test_tweets.csv")

print(train_tweet.shape, test_tweet.shape)


# In[ ]:

## Event rate
print("Event Rate -",round(train_tweet['label'].sum()/train_tweet['label'].count()*100,2),"%")


# #####   1.  Data Cleaning (eg: remove special characters, lower, blank, NA ..)

# In[ ]:

## Copy data
train_tweet1 = train_tweet

## Remove Special Characters, digits from python Dataframe column
train_tweet1['tweet_updated'] = train_tweet1["tweet"].str.replace('[^\w\s\d]','')
train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].str.replace("\s+", " ")
train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].str.replace('\d+', '')

## Remove NA and Blank
train_tweet1 = train_tweet[train_tweet["tweet_updated"] != ""]
train_tweet1 = train_tweet1[train_tweet1["tweet_updated"] != " "]
train_tweet1 = train_tweet1.dropna(axis='rows')

## Convert into lower case
train_tweet1["tweet_updated"] = train_tweet1["tweet_updated"].map(lambda x: x.lower())

## Remove low lenth rows
train_tweet1["Len"] = train_tweet1["tweet_updated"].str.len()
train_tweet1 = train_tweet1[["id","label","tweet_updated"]][train_tweet1["Len"]>=3]

## Check the data
print(train_tweet1.head(7), train_tweet1.shape)


# #### 2. Remove strong words

# In[ ]:

## Removing strong words from data to have unbiased prediction
#strong_words=['fight',"versace"]

## Remove the rows
#train_tweet1=train_tweet1[~train_tweet1['tweet_updated'].str.contains('|'.join(strong_words),case=False)]

## Replace the words only
replace_words = {
   'tweet_updated': {
      r'(fight|versace)': ''}
}

train_tweet1.replace(replace_words, regex=True, inplace=True)
train_tweet1.shape


# #### 3. Abbrevations & Spell check

# In[ ]:

## Spell chevk and Abbrevation correction   
global replace_text

class WordReplacer(object):
 
    def __init__(self, word_map):
            self.word_map = word_map
    def replace(self, word):
         return self.word_map.get(word, word)
 
 
def replace_text(text):
    tokenized_text = word_tokenize(text.lower())
    replaced_text=[replacer.replace(word) for word in tokenized_text ]
    replaced_text =' '.join(replaced_text)
    return replaced_text
 
## Create word map
    words_maping={'bihday' : 'birthday','mrng':'morning',
                  'tq':'thank you', 'rep':'repeat'}
    
## replace words
    replacer=WordReplacer(words_maping)
    train_tweet1['tweet_updated']=train_tweet1['tweet_updated'].apply(replace_text)


# #### 4. Tokenazation & Remove stop words

# In[ ]:

## Remove stop words
stop_words = stopwords.words('english')
remove_stop=['before', 'after', 'above']
stop_words= [word for word in stop_words if word  not in remove_stop]

## Add to stop words
stop_words.extend(['your','yours','yourself','yourselves'])
 
## Total Sto words
print("Total number of customized stopwords is ",len(stop_words))


# In[ ]:

## Clean text
def clean_text(text):
    tokenized_text = word_tokenize(text.lower())
    cleaned_text =[k for k in tokenized_text if k not in stop_words and re.match('[a-zA-Z\\-][a-zA-Z\\-]{2,}', k)]
    cleaned_text =' '.join(cleaned_text)
    return cleaned_text
 
train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].apply(clean_text)
train_tweet1['tweet_updated'].head(7)


# #### 5. Lemetization & stemming

# In[ ]:

## lemetization of the text
lmtzr = WordNetLemmatizer()
def lemetization(text):      
    tokenized_text = word_tokenize(text.lower())   
    clean_text =[lmtzr.lemmatize(k,pos='v') for k in tokenized_text]  
    clean_text =' '.join(clean_text)
    return clean_text
 
train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].apply(lemetization)


# In[ ]:

## stemming of the text
porter = PorterStemmer()
def stemSentence(sentence):
    token_words=word_tokenize(sentence)
    token_words
    stem_sentence=[]
    for word in token_words:
        stem_sentence.append(porter.stem(word))
        stem_sentence.append(" ")
    return "".join(stem_sentence)

train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].apply(stemSentence) 


# In[ ]:

train_tweet1.head(3)


# #### 6. synonyms removal from text

# #### 7. Common and Rare words removal

# In[ ]:

## Max freq words - Run Once
freq_max = pd.Series(' '.join(train_tweet1['tweet_updated']).split()).value_counts()[:1] # change based on requirement
freq_max


# In[ ]:

## Max freq words - Run Once
freq_min = pd.Series(' '.join(train_tweet1['tweet_updated']).split()).value_counts()[-100:] # change based on requirement
freq_min


# In[ ]:

## Remove max freq words
freq_max = list(freq_max.index)
freq_min = list(freq_min.index)

train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].apply(lambda x: " ".join(x for x in x.split() if x not in freq_max))
train_tweet1['tweet_updated'] = train_tweet1['tweet_updated'].apply(lambda x: " ".join(x for x in x.split() if x not in freq_min))


# In[ ]:

train_tweet1.head(3)


# #### 8. Count Vectorization ( Chi-square feature extraction)

# In[ ]:

## Unigram - count vectorization
CountVectorizer_unigram = CountVectorizer(ngram_range=(1,1),max_features=2000) #max_features based on term_freq #binary=True/False

## Bigram - count vectorization
CountVectorizer_bigram = CountVectorizer(ngram_range=(2,2),max_features=2000) #max_features based on term_freq #binary=True/False

## Fit count vectorization & transform into table
CountVectorizer_unigram.fit(train_tweet1['tweet_updated'])
unigrams = CountVectorizer_unigram.transform(train_tweet1['tweet_updated'])

CountVectorizer_bigram.fit(train_tweet1['tweet_updated'])
bigrams = CountVectorizer_bigram.transform(train_tweet1['tweet_updated'])

## Get Count vectorized data
unigram_data=pd.DataFrame(unigrams.toarray(),columns=CountVectorizer_unigram.get_feature_names())
bigram_data=pd.DataFrame(bigrams.toarray(),columns=CountVectorizer_bigram.get_feature_names())

## Add target variable
final_data1 = pd.concat([train_tweet1[['id','label']],unigram_data], axis=1)
final_data2 = pd.concat([train_tweet1[['id','label']],bigram_data], axis=1)

print(final_data1.shape,final_data2.shape)


# In[ ]:

## Select k-th grams by chi-square
Nselect_unigram=SelectKBest(chi2,k=50)
Nselect_unigram.fit(unigrams,train_tweet1["label"])
Nbest_unigram=Nselect_unigram.transform(unigrams)

# Get the sleccted metrics by SelectKbest
support = Nselect_unigram.get_support()
new_features_unigram = []
 
for bool, feature in zip(support, CountVectorizer_unigram.get_feature_names()):
    if bool:
        new_features_unigram.append(feature)
        unigrams = pd.DataFrame(new_features_unigram)

        
# Selected grams
NBest_unigram_data=pd.DataFrame(Nbest_unigram.toarray(),columns=new_features_unigram)
NBest_unigram_data.shape


# In[ ]:

## Select n-th grams by chi-square method tfidf_kbest_2_gram
Nselect_bigram=SelectKBest(chi2,k=20)
Nselect_bigram.fit(bigrams,train_tweet1["label"])
Nbest_bigram=Nselect_bigram.transform(bigrams)

# Get the sleccted metrics by SelectKbest
support = Nselect_bigram.get_support()
new_features_bigram = []
 
for bool, feature in zip(support, CountVectorizer_bigram.get_feature_names()):
    if bool:
        new_features_bigram.append(feature)
        bigrams = pd.DataFrame(new_features_bigram)

        
# Selected grams
NBest_bigram_data=pd.DataFrame(Nbest_bigram.toarray(),columns=new_features_bigram)
NBest_bigram_data.shape


# In[ ]:

## Check all bigrams & unigrams from business sense 
## Concat data
text_model = pd.concat([train_tweet1["label"],NBest_unigram_data,NBest_bigram_data], axis=1)
#text_model.to_csv(r"C:\Users\DEMO\Desktop\Jai Sri Krishna\Python\output/text_data.csv")
text_model.shape


# #### 9. Build a predictive model on that text data

# In[ ]:

## Data split
X = text_model.drop(['label'], axis=1)
y = text_model["label"]
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2,random_state=7)
print(x_train.shape,x_test.shape)


# In[ ]:

## Run randomforest
rf_int = RandomForestClassifier(n_estimators = 500, random_state = 42,class_weight= 'balanced',
                           criterion='gini',max_depth=7,max_features='sqrt')
# Train the model on training data
rf_int.fit(x_train, y_train)


# In[ ]:

# List of features for later use
feature_list = list(x_train.columns)
# Get numerical feature importances
importances = list(rf_int.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances 
important_var = pd.DataFrame(feature_importances)
# Rename column
important_var.rename(columns={0:'Variables', 1:'Importance'}, inplace=True)
#important_var.to_csv(r"C:\Users\DEMO\Desktop\Jai Sri Krishna\Python\output/var_imp.csv")
important_var


# In[ ]:

## Select siginificant bigrams and unigrams
predictors = ['trump','white','allahsoil','black','racist','amp','obama','women','liber','racism','libtard','polit',
              'sjw','bigot','blm','comment','hate','ignor','listen','retweet','stomp','amp feel','liber polit',    
              'libtard libtard','libtard sjw','listen retweet','might libtard','sjw liber','stomp listen']


# In[ ]:

## Re-Run randomforest
rf = RandomForestClassifier(n_estimators = 500, random_state = 42,class_weight= 'balanced',
                           criterion='gini',max_depth=7,max_features='sqrt')
# Train the model on training data
rf.fit(x_train[predictors], y_train)


# In[ ]:

## predicted probability on text data
pred1 = rf.predict_proba(x_test[predictors])


# In[ ]:

# Get confusion metrics and AUC, Recall, Accuracy & Precision score
def print_metrics(target,prob,threshold,event):
    " target= ytest, prob= predicted probability, threshold= cut of value "
    data= pd.DataFrame()
    data["pred_proba"]= pd.Series(prob)
    data["pred"]= data["pred_proba"].map(lambda x:1.0 if x>= threshold else 0.0)
    print("Threshold Probability:",threshold )
    print(metrics.confusion_matrix(target,data['pred']))
    precision = metrics.precision_score(target,data['pred'])
    recall = metrics.recall_score(target,data['pred'])
    f1_score = metrics.f1_score(target,data['pred'])
    auc_score = metrics.roc_auc_score(target,data['pred'])
    event_rate = round(event.sum()/event.count()*100,2)
    print("Accuracy: % .4g " % metrics.accuracy_score(target,data['pred']))
    print("Precision:",round(precision,3))
    print("Recall:",round(precision,3))
    print("F1_Score:",round(precision,3))
    print("AUC Score:",round(auc_score,3))
    print("Event Rate:",event_rate,"%")
    
print_metrics(y_test,pred1[:,1],0.5,y_test)


# #### 10. Testing with new data - Cleaning step wise & Prediction

# In[ ]:

## Test data cleaning

## A. Remove Special Characters, digits from python Dataframe column
test_tweet['tweet_updated'] = test_tweet["tweet"].str.replace('[^\w\s\d]','')
test_tweet['tweet_updated'] = test_tweet['tweet_updated'].str.replace("\s+", " ")
test_tweet['tweet_updated'] = test_tweet['tweet_updated'].str.replace('\d+', '')

## B. Remove NA and Blank
test_tweet = test_tweet[test_tweet["tweet_updated"] != ""]
test_tweet = test_tweet[test_tweet["tweet_updated"] != " "]
test_tweet = test_tweet.dropna(axis='rows')

## C. Convert into lower case
test_tweet["tweet_updated"] = test_tweet["tweet_updated"].map(lambda x: x.lower())

## D. Strong words removal
test_tweet.replace(replace_words, regex=True, inplace=True)

# E. Abbreviation and Spell check
def replace_text(text):
    tokenized_text = word_tokenize(text.lower())
    replaced_text=[replacer.replace(word) for word in tokenized_text ]
    replaced_text =' '.join(replaced_text)
    return replaced_text
wordmap={'bihday' : 'birthday','mrng':'morning','tq':'thank you', 'rep':'repeat'}
replacer=WordReplacer(wordmap)
test_tweet['tweet_updated']=test_tweet['tweet_updated'].apply(replace_text)

## F. Stop words remove
test_tweet['tweet_updated'] = test_tweet['tweet_updated'].apply(clean_text)

## G. Lemmetization and stemmering
test_tweet['tweet_updated'] = test_tweet['tweet_updated'].apply(lemetization)
test_tweet['tweet_updated'] = test_tweet['tweet_updated'].apply(stemSentence)

## H. Synonyms correction
#test_tweet['tweet_updated']=test_tweet['tweet_updated'].apply(replace_text)


# In[ ]:

### Create Variables for Testing ###
test_unigram=CountVectorizer_unigram.transform(test_tweet['tweet_updated'])
test_unigram_data=pd.DataFrame(test_unigram.toarray(),columns=CountVectorizer_unigram.get_feature_names())

test_bigram=CountVectorizer_bigram.transform(test_tweet['tweet_updated'])
test_bigram_data=pd.DataFrame(test_bigram.toarray(),columns=CountVectorizer_bigram.get_feature_names())


### Create final data for testing
concat_grams = pd.concat([test_unigram_data,test_bigram_data],axis=1)
final_test = concat_grams[predictors]
                
print(temp_count1.shape,temp_count2.shape,concat_grams.shape,final_test.shape)


# In[ ]:

## Predict on Testing
Pred_Proba = pd.DataFrame(rf.predict_proba(final_test[predictors]),columns=["Predict_0","Predict_1"])
test_predicted = pd.concat([test_tweet,Pred_Proba],axis=1)
test_predicted["Predict_class"] = np.where(test_predicted["Predict_1"]>=0.5,1,0)


# #### 11. TF-IDF calculation

# In[ ]:

## Calculate TF-IDF score vectorizer_tfidf_1_gram
tfidf_uni_bi_gram = TfidfVectorizer(ngram_range=(1,2),max_features=2000) #max_features based on term_freq

tfidf_uni_bi_gram.fit(train_tweet1['tweet_updated'])
tfidf_1_2_gram=tfidf_uni_bi_gram.transform(train_tweet1['tweet_updated'])

tfidf_data=pd.DataFrame(tfidf_1_2_gram.toarray(),columns=tfidf_uni_bi_gram.get_feature_names())

final_data1 = pd.concat([train_tweet1[['id','label']],tfidf_data], axis=1)
final_data1.shape


# In[ ]:

final_data1.head(7)


# #### 12. Topic Model - Pending

# In[ ]:

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.decomposition import NMF, LatentDirichletAllocation

def display_topics(model, feature_names, no_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print "Topic %d:" % (topic_idx)
        print " ".join([feature_names[i]
                        for i in topic.argsort()[:-no_top_words - 1:-1]])

dataset = fetch_20newsgroups(shuffle=True, random_state=1, remove=('headers', 'footers', 'quotes'))
documents = dataset.data

no_features = 1000

# NMF is able to use tf-idf
tfidf_vectorizer = TfidfVectorizer(max_df=0.95, min_df=2, max_features=no_features, stop_words='english')
tfidf = tfidf_vectorizer.fit_transform(documents)
tfidf_feature_names = tfidf_vectorizer.get_feature_names()

# LDA can only use raw term counts for LDA because it is a probabilistic graphical model
tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2, max_features=no_features, stop_words='english')
tf = tf_vectorizer.fit_transform(documents)
tf_feature_names = tf_vectorizer.get_feature_names()

no_topics = 20

# Run NMF
nmf = NMF(n_components=no_topics, random_state=1, alpha=.1, l1_ratio=.5, init='nndsvd').fit(tfidf)

# Run LDA
lda = LatentDirichletAllocation(n_topics=no_topics, max_iter=5, learning_method='online', learning_offset=50.,random_state=0).fit(tf)

no_top_words = 10
display_topics(nmf, tfidf_feature_names, no_top_words)
display_topics(lda, tf_feature_names, no_top_words)


# #### ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
