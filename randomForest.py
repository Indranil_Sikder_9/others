
# coding: utf-8

# ### ---------------------------------------------------------      RandomForest    -------------------------------------------------------------------

# In[ ]:

# Import Required packages
import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
import warnings
warnings.filterwarnings('ignore')
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import matplotlib.pyplot as plt
import pylab as p
from pylab import rcParams
from termcolor import colored
import pandas_profiling as pp


# #### 1.  Load data & basic structure checks

# In[ ]:

# Import basedata & out of time data
masterdata = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Data/masterdata1.csv")
out_time = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Data/out_time.csv")
print(masterdata.shape,out_time.shape)


# In[ ]:

# Check data type for all variables
pp.ProfileReport(masterdata)


# #### 2. Split the data in Train and Validation1, Validation2

# In[ ]:

# Split into train test
X = masterdata.drop(['Cust_char'], axis=1)
y = masterdata["Cust_char"]
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.4,random_state=7)

Xx = x_test
yy = y_test
x_validation1, x_validation2, y_validation1, y_validation2 = train_test_split(Xx, yy, test_size=0.5,random_state=7)

print(x_train.shape,x_validation1.shape,x_validation2.shape)

# Remove Customer_ID
x_train = x_train.drop('Customer_ID', 1)


# In[ ]:

# Remove dataset from python memory
del masterdata, x_test, y_test


# #### 3. Grid Search randomFoest - optimal parameters

# In[ ]:

# Create the parameters for grid search
rfc=RandomForestClassifier(random_state=42)

param_grid = { 
    'n_estimators': [200,300,400,500,600],
    'max_features': ['auto', 'sqrt', 'log2'],
    'max_depth' : [4,5,6,7,8],
    'criterion' :['gini', 'entropy'],
    'class_weight' : [None,'balanced']
}
CV_rfc = GridSearchCV(estimator=rfc, param_grid=param_grid, cv= 5)
CV_rfc.fit(x_train, y_train)

# Best parameter
CV_rfc.best_params_


# #### 4. Model iterations & Important variables

# In[ ]:

# Run Model1 with Optimum paratmeters
rf = RandomForestClassifier(n_estimators = 500, random_state = 42,class_weight= 'balanced',
                           criterion='gini',max_depth=7,max_features='sqrt')
# Train the model on training data
rf.fit(x_train, y_train)


# In[ ]:

# List of features for later use
feature_list = list(x_train.columns)
# Get numerical feature importances
importances = list(rf.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances 
important_var = pd.DataFrame(feature_importances)
# Rename column
important_var.rename(columns={0:'Variables', 1:'Importance'}, inplace=True)
important_var


# In[ ]:

# list of x locations for plotting
x_values = list(range(len(importances)))
# Make a bar chart
plt.bar(x_values, importances, orientation = 'vertical', color = 'r', edgecolor = 'k', linewidth = 1.2)
# Tick labels for x axis
plt.xticks(x_values, feature_list, rotation='vertical')
# Axis labels, title & plot size
plt.ylabel('Importance')
plt.xlabel('Variable')
plt.title('Variable Importances')
p.show()

# Change figure size
rcParams['figure.figsize'] = 10, 10


# In[ ]:

# List of features sorted from most to least important
sorted_importances = [importance[1] for importance in feature_importances]
sorted_features = [importance[0] for importance in feature_importances]
# Cumulative importances
cumulative_importances = np.cumsum(sorted_importances)
# Make a line graph
plt.plot(x_values, cumulative_importances, 'g-')
# Draw line at 95% of importance retained
plt.hlines(y = 0.95, xmin=0, xmax=len(sorted_importances), color = 'r', linestyles = 'dashed')
# Format x ticks and labels
plt.xticks(x_values, sorted_features, rotation = 'vertical')
# Axis labels and title
plt.xlabel('Variable')
plt.ylabel('Cumulative Importance')
plt.title('Cumulative Importances')
p.show()


# In[ ]:

# Find number of features for cumulative importance of 99%
# Add 1 because Python is zero-indexed
print('Number of features for 95% importance:', np.where(cumulative_importances > 0.99)[0][0] + 1)


# In[ ]:

# Select Important Columns
predictors = ["total_rev_hi_lim","dti","installment","tot_cur_bal","int_rate","LoanVSinc.","no.derg",
                 "no.delqn","list.f","pur.debt","term.36","grade.a","grade.b","grade.c","grade.d","grade.e","grade.f",
                 "home_rent","home_own","home_mortg","emp..1","emp.1","emp.2","emp.3","emp.4","emp.5","emp.6","emp.7",
                 "emp.9","emp.10.","pur.cc","pur.home","pur.major","pur.other"]


# In[ ]:

# Run the final Model with siginifacnt variable
# Run Model1 with Optimum paratmeters
rf_final = RandomForestClassifier(n_estimators = 500, random_state = 42,class_weight= 'balanced',
                           criterion='gini',max_depth=7,max_features='sqrt')
# Train the model on training data
rf_final.fit(x_train[predictors], y_train)


# #### 5. Validation of model on different data (Confusion metrics, AUC & optimal cut off)

# In[ ]:

# Predict for Validation1, Validation2 & outtime
pred1 = rf_final.predict_proba(x_validation1[predictors])
pred2 = rf_final.predict_proba(x_validation2[predictors])
ot = rf_final.predict_proba(out_time[predictors])


# In[ ]:

# Get confusion metrics and AUC, Recall, Accuracy & Precision score
def print_all_metrics(target,prob,threshold,event):
    " target= ytest, prob= predicted probability, threshold= cut of value "
    data= pd.DataFrame()
    data["pred_proba"]= pd.Series(prob)
    data["pred"]= data["pred_proba"].map(lambda x:1.0 if x>= threshold else 0.0)
    print("Threshold Probability:",threshold )
    print(metrics.confusion_matrix(target,data['pred']))
    precision = metrics.precision_score(target,data['pred'])
    recall = metrics.recall_score(target,data['pred'])
    f1_score = metrics.f1_score(target,data['pred'])
    auc_score = metrics.roc_auc_score(target,data['pred'])
    event_rate = round(event.sum()/event.count()*100,2)
    print("Accuracy: % .4g " % metrics.accuracy_score(target,data['pred']))
    print("Precision:",round(precision,3))
    print("Recall:",round(precision,3))
    print("F1_Score:",round(precision,3))
    print("AUC Score:",round(auc_score,3))
    print("Event Rate:",event_rate,"%")


# In[ ]:

# Get results of all metrics
print(colored("Validation1 Sample:-",'blue'))
print("Observations",y_validation1.count())
print_all_metrics(y_validation1,pred1[:,1],0.46,y_validation1)
print("-------------------------------------------------")
print(colored("Validation2 Sample:-",'blue'))
print("Observations",y_validation2.count())
print_all_metrics(y_validation2,pred2[:,1],0.46,y_validation2)
print("-------------------------------------------------")
print(colored("Out Time Sample:-",'blue'))
print("Observations",out_time["Cust_char"].count())
print_all_metrics(out_time["Cust_char"],ot[:,1],0.46,out_time["Cust_char"])


# In[ ]:

# Specificity and Sensitivity plot for optimum cut off
## tpr- true positive rate, fpr- false positive rate.
def optimum_cutoff(x,y):
    fpr, tpr, thresholds = metrics.roc_curve(x, y)
    roc_auc = metrics.auc(fpr, tpr)
    print("Area under the ROC curve : %f" % roc_auc)

    ####################################
    # The optimal cut off would be where tpr is high and fpr is low
    # tpr - (1-fpr) is zero or near to zero is the optimal cut off point
    ####################################
    
    i = np.arange(len(tpr)) # index for df
    roc = pd.DataFrame({'fpr' : pd.Series(fpr, index=i),'tpr' : pd.Series(tpr, index = i), '1-fpr' : pd.Series(1-fpr, index = i), 'tf' : pd.Series(tpr - (1-fpr), index = i), 'thresholds' : pd.Series(thresholds, index = i)})
    roc.ix[(roc.tf-0).abs().argsort()[:1]]

    # Plot tpr vs 1-fpr
    fig, ax = plt.subplots()
    plt.plot(roc['tpr'])
    plt.plot(roc['1-fpr'], color = 'red')
    plt.xlabel('1-False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    ax.set_xticklabels([])
    p.show()


# In[ ]:

# Get Optimum Cutoff
optimum_cutoff(y_validation1,pred1[:,1])
optimum_cutoff(y_validation2,pred2[:,1])


# ## ------------------------------------------------------------------------------------------------------------------------------
