
# coding: utf-8

# ### ---------------------------------------------------------      Logistic Regression    -------------------------------------------------------------------

# In[ ]:

# Import Required packages
import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
import matplotlib.pyplot as plt
import pylab as p
from pylab import rcParams
from termcolor import colored
import pandas_profiling as pp
from sklearn.linear_model import LogisticRegression
import warnings
warnings.filterwarnings('ignore')


# #### 1.  Load data & basic structure checks

# In[ ]:

# Import basedata & out of time data
masterdata = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Data/masterdata1.csv")
out_time = pd.read_csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Python/Data/out_time.csv")
print(masterdata.shape,out_time.shape)


# In[ ]:

# Check data type for all variables
pp.ProfileReport(masterdata)


# #### 2. Split the data in Train and Validation1, Validation2

# In[ ]:

# Split into train test
X = masterdata.drop(['Cust_char'], axis=1)
y = masterdata["Cust_char"]
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.4,random_state=7)

Xx = x_test
yy = y_test
x_validation1, x_validation2, y_validation1, y_validation2 = train_test_split(Xx, yy, test_size=0.5,random_state=7)

print(x_train.shape,x_validation1.shape,x_validation2.shape)

# Remove Customer_ID
x_train = x_train.drop('Customer_ID', 1)


# In[ ]:

# Remove dataset from python memory
del masterdata, x_test, y_test


# #### 3. Grid Search Logistic - optimal parameters

# In[ ]:

# Create the parameters for grid search
grid={"C":np.arange(0.1,2,0.1),
      "penalty":["l1"], # l1 lasso l2 ridge 
      "class_weight" : [{ 0:0.67, 1:0.33 }, 'balanced', None, { 0:0.7, 1:0.3 }, { 0:0.65, 1:0.35 }],
      "max_iter": [100,200,300],
      "fit_intercept": [True,False],
      "solver" : ['liblinear'], #'lbfgs','newton-cg', 'saga'
      "intercept_scaling":np.arange(0.1,2,0.1),
      # Intercept useful only when the solver ‘liblinear’ is used
      } 

# ‘newton-cg’, ‘lbfgs’ and ‘sag’ only handle L2 penalty, whereas ‘liblinear’ and ‘saga’ handle L1 penalty.

# Run Grid Search
logreg=LogisticRegression(random_state=7)
logreg_cv=GridSearchCV(logreg,grid,cv=5)
logreg_cv.fit(x_train,y_train)

# Best parameter
logreg_cv.best_params_


# #### 4. Run Optimised model

# In[ ]:

# Run Model
logistic = LogisticRegression(C=0.3,class_weight="balanced",fit_intercept=True,intercept_scaling=0.1,
                             max_iter=150,penalty="l1",solver='liblinear',random_state=7)
logistic.fit(x_train,y_train)


# #### 5. Validation of model on different data (Confusion metrics, AUC & optimal cut off)

# In[ ]:

# Take predictor columns
predictors= x_train.columns


# In[ ]:

# Predict for Validation1, Validation2 & outtime
pred1 = logistic.predict_proba(x_validation1[predictors])
pred2 = logistic.predict_proba(x_validation2[predictors])
ot = logistic.predict_proba(out_time[predictors])


# In[ ]:

# Get confusion metrics and AUC, Recall, Accuracy & Precision score
def print_all_metrics(target,prob,threshold,event):
    " target= ytest, prob= predicted probability, threshold= cut of value "
    data= pd.DataFrame()
    data["pred_proba"]= pd.Series(prob)
    data["pred"]= data["pred_proba"].map(lambda x:1.0 if x>= threshold else 0.0)
    print("Threshold Probability:",threshold )
    print(metrics.confusion_matrix(target,data['pred']))
    precision = metrics.precision_score(target,data['pred'])
    recall = metrics.recall_score(target,data['pred'])
    f1_score = metrics.f1_score(target,data['pred'])
    auc_score = metrics.roc_auc_score(target,data['pred'])
    event_rate = round(event.sum()/event.count()*100,2)
    print("Accuracy: % .4g " % metrics.accuracy_score(target,data['pred']))
    print("Precision:",round(precision,3))
    print("Recall:",round(precision,3))
    print("F1_Score:",round(precision,3))
    print("AUC Score:",round(auc_score,3))
    print("Event Rate:",event_rate,"%")


# In[ ]:

# Get results of all metrics
print(colored("Validation1 Sample:-",'blue'))
print("Observations",y_validation1.count())
print_all_metrics(y_validation1,pred1[:,1],0.46,y_validation1)
print("-------------------------------------------------")
print(colored("Validation2 Sample:-",'blue'))
print("Observations",y_validation2.count())
print_all_metrics(y_validation2,pred2[:,1],0.46,y_validation2)
print("-------------------------------------------------")
print(colored("Out Time Sample:-",'blue'))
print("Observations",out_time["Cust_char"].count())
print_all_metrics(out_time["Cust_char"],ot[:,1],0.46,out_time["Cust_char"])


# In[ ]:

# Specificity and Sensitivity plot for optimum cut off
## tpr- true positive rate, fpr- false positive rate.
def optimum_cutoff(x,y):
    fpr, tpr, thresholds = metrics.roc_curve(x, y)
    roc_auc = metrics.auc(fpr, tpr)
    print("Area under the ROC curve : %f" % roc_auc)

    ####################################
    # The optimal cut off would be where tpr is high and fpr is low
    # tpr - (1-fpr) is zero or near to zero is the optimal cut off point
    ####################################
    
    i = np.arange(len(tpr)) # index for df
    roc = pd.DataFrame({'fpr' : pd.Series(fpr, index=i),'tpr' : pd.Series(tpr, index = i), '1-fpr' : pd.Series(1-fpr, index = i), 'tf' : pd.Series(tpr - (1-fpr), index = i), 'thresholds' : pd.Series(thresholds, index = i)})
    roc.ix[(roc.tf-0).abs().argsort()[:1]]

    # Plot tpr vs 1-fpr
    fig, ax = plt.subplots()
    plt.plot(roc['tpr'])
    plt.plot(roc['1-fpr'], color = 'red')
    plt.xlabel('1-False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    ax.set_xticklabels([])
    p.show()


# In[ ]:

# Get Optimum Cutoff
optimum_cutoff(y_validation1,pred1[:,1])
optimum_cutoff(y_validation2,pred2[:,1])


# ## --------------------------------------------------------------------------------------------------------------------------------
