######################################################################################
########################         DATA MANUPULATION              ######################
######################################################################################


# Required Library
library(caret)
library(DMwR)
library(outliers)
library(mice)
library(VIM)
library(missForest)
library(MASS)
library(car)
library(mlbench)
library(plyr)


################################   CLASS IMBALANCE   ###############################

# Import Data
mydata  = read.csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Data Set/trainDF.csv") 

# Remove Variabls
mydata$X.1 = NULL
mydata$X = NULL

# Target
table(mydata$Cust_char) #19.82% as 0 Class ~ Cust_char

# Change Data Type
mydata$Cust_char = as.factor(mydata$Cust_char)

# Down Sampling
set.seed(100)
down_train <- data.frame(downSample(x = mydata[, colnames(mydata[,2:49])],
                         y = mydata$Cust_char))
table(down_train$Class)

# Up Sampling
set.seed(100)
up_train <- data.frame(upSample(x = mydata[, colnames(mydata[,2:49])],
                     y = mydata$Cust_char))
table(up_train$Class)

# SMOTE
set.seed(365)
smote_train <- SMOTE(Cust_char ~ ., data  = mydata,perc.over = 100,perc.under = 100)                         
table(smote_train$Cust_char)

#######################         OUTLIER DETECTION             #######################

#This function performs a simple test for one outlier
chisq.out.test(mydata$total_rev_hi_lim,variance = var(mydata$total_rev_hi_lim))

#Finds value with largest difference between
#it and sample mean, which can be an outlier.
outlier(mydata$total_rev_hi_lim, opposite = FALSE, logical = FALSE)

#If the outlier is detected and confirmed by statistical tests,
#this function can remove it or replace bysample mean or median.
mydata$total_rev_hi_lim=rm.outlier(mydata$total_rev_hi_lim,fill = TRUE,
                                   median = TRUE,opposite = FALSE)


###########################     MISSING IMPUTATION    #############################

mydata = read.csv("C:/Users/DEMO/Desktop/Jai Sri Krishna/Data Set/Dataset1.csv")

#Summary of Data
summary(mydata)

# Factor Variable
mydata$Factor = as.factor(rep(c(1,2,3,NA,1,2,1,3,1,3),times=10))

# Change Data Type
mydata$Response = as.factor(mydata$Response)

################### MICE  ######################

#tabuler form of missing
md.pattern(mydata)

#Graphical form of missing
mice_plot <- aggr(mydata, col=c('navyblue','yellow'),
                  numbers=TRUE, sortVars=TRUE,
                  labels=names(mydata), cex.axis=.7,
                  gap=3, ylab=c("Missing data","Pattern"))

#Impute missing values
init = mice(mydata, maxit=0) 
meth = init$method
predM = init$predictorMatrix

#Remove the variable as a predictor but still will be imputed
predM[, c("Response")]=0

#Skip a variable from imputation but will be used for prediction
meth[c("Age")]=""

#Specify the methods for imputing the missing values of each variables 
meth[c("dti")]="pmm" 
meth[c("installment")]="mean" 
meth[c("int_rate")]="pmm"
meth[c("LoanVSinc.")]="norm"
meth[c("Age")]="pmm"
meth[c("Response")]="logreg" 
meth[c("Factor")]="polyreg"

#Run MICE
set.seed(123)
imputed = mice(mydata, method=meth, predictorMatrix=predM, m=5,maxit = 50)

#Create A Dataset after imputation
imputed_data <- complete(imputed,2)

################### missForest  ######################

# Missing
summary(mydata)

# missForest
missForest_imputed = missForest(mydata)
missForest_imputed

# Imputed Data
miss_imputed = missForest_imputed$ximp


############################   CHI-SQUARE & ANOVA TEST ###############################

#Data
chi_data = Cars93
str(chi_data)

# Create Table
t = table(chi_data$Type,chi_data$Cylinders)
t

#Chi_square Test
chisq.test(t)

# ANOVA TEST
aov(chi_data$Price ~ chi_data$Type)

# Correlation Cut off
findCorrelation(cormatrix,cutoff = 0.5)

########################    Data Manupulation    ##################################

# Get First & Last Row
head = aggregate(mydata,by=list(mydata$ID),FUN=function(x) {First = head(x,1)})
tail = aggregate(mydata,by=list(mydata$ID),FUN=function(x) {Last = tail(x,1)})

# Remove Duplicate
Unique_Data = mydata[!duplicated(mydata$ID),]

# Paste by rows
Text_Data = data.frame(aggregate(mydata$text, list(mydata$group), paste, collapse=""))

# White Space in R
mydata$var = gsub(" ",NA,mydata$var,fixed = T)
mydata$var = sub("^$",NA,mydata$var)

# Cumulative sum
cumsum_data = ddply(mydata_order,.(ClaimNo),transform,cumsum=cumsum(Cheq_amt))

# Read Scientific numbers
options(scipen = 999)

###################################################################################