/*Behavioural Modeling*/

/*Importing data*/
libname in "C:\Users\Indraj\Desktop\Sas.files\SAS Modeling (C.C.)\Datasets";

/*To check the overall structure of the data*/
proc contents data = in.Data_logistic;
run;

/*To see the percentage distribution of the target variable*/
proc freq data = in.Data_logistic;
table Is_Good;
run;

/*To check if this is a good target variable or not and whether 0 is good or 1*/
proc freq data = in.Data_Logistic;
table Is_Good*Income_Group/ norow nocol;
run;

proc freq data = in.Data_Logistic;
table Is_Good*Residential_Status/ norow nocol;
run;
proc freq data = in.Data_Logistic;
table Is_Good*Residential_Status*Income_Group/ norow nocol;
run;

/*Since the percentage of good should usually be more, we take 0as good 1 as bad*/

/*To create a macro to check proc freq of the categorical variables*/
proc freq data = in.Data_Logistic ;
table Is_Good/ norow nocol ;
run;

%macro Pfreq(Var);
proc freq data = in.Data_Logistic ;
table &var./ norow nocol ;
run;
%mend Pfreq;
 
%Pfreq(Residential_Status);
%Pfreq(Is_Good);
%Pfreq(Number_Creditcard);
%Pfreq(Have_Web_Login);
%Pfreq(Have_Car);
%Pfreq(Occupation_Type);
%Pfreq(Income_Group);
%Pfreq(Qualification);
%Pfreq(Vintage);
%Pfreq(Gender);
%Pfreq(Bank_Accounts);
%Pfreq(Pancard_Ind);

/*for continuos variables*/
%macro Uni(Var);
proc univariate data = in.Data_Logistic;
Var &var.;
run;
%mend Uni;

%Uni(Credit_Limit);
%Uni(Income_other_Source);

/*for discrete variable calculating woe*/
/*******************************WOE Calculation********************************/
proc sql;
/*create table tt as */
select 
sum( case when Is_Good eq 1 then 1 else 0 end), 
sum(case when Is_Good eq 0 then 1 else 0 end) into :tot_good,:tot_bad
from in.Data_Logistic;
quit;
proc sql;
create table Residential_Status_tt as
select  Residential_Status,
sum( case when Is_Good eq 1 then 1 else 0 end) as num_goods,
sum(case when Is_Good eq 0 then 1 else 0 end) as num_bads,
calculated num_goods/&tot_good as percent_goods,
calculated num_bads/&tot_bad as percent_bads,
log(calculated percent_goods/calculated percent_bads) as woe_var,
(calculated percent_goods-calculated percent_bads) as diff_goods_bads,
calculated diff_goods_bads*calculated woe_var as IV_var
from tt
group by Residential_Status;
quit;
/*creating using Macro*/
%macro woe(var);
proc sql;
/*create table tt as */
select 
sum( case when Is_Good eq 1 then 1 else 0 end), 
sum(case when Is_Good eq 0 then 1 else 0 end) into :tot_good,:tot_bad
from in.Data_Logistic;
quit;

proc sql;
create table &var._tt as
select  &var.,
sum( case when Is_Good eq 1 then 1 else 0 end) as num_goods,
sum(case when Is_Good eq 0 then 1 else 0 end) as num_bads,
calculated num_goods/&tot_good as percent_goods,
calculated num_bads/&tot_bad as percent_bads,
log(calculated percent_goods/calculated percent_bads) as woe_var,
(calculated percent_goods-calculated percent_bads) as diff_goods_bads,
calculated diff_goods_bads*calculated woe_var as IV_var
from in.Data_Logistic
group by &var.;
quit;
%mend woe;

%woe(Residential_Status);
%woe(Number_Creditcard);
%woe(Have_Web_Login);
%woe(Have_Car);
%woe(Occupation_Type);
%woe(Income_Group);
%woe(Qualification);
%woe(Vintage);
%woe(Gender);
%woe(Bank_Accounts);
%woe(Pancard_Ind);
/*******************Fine Classing of Continuous Variable****************/
/*for continuous variables*/
%macro woe_c(var);

proc sql;
/*create table tt as */
select 
sum( case when Is_Good eq 1 then 1 else 0 end), 
sum(case when Is_Good eq 0 then 1 else 0 end) into :tot_good,:tot_bad
from in.Data_Logistic;
quit;

proc rank data=in.Data_Logistic groups=10 out=project;
var &var.;
ranks &var._Rank;
run; 
proc sql;
create table &var._rank as
select  &var._rank,min(&var.) as &var._min, max(&var.) as &var._max,
sum( case when Is_Good eq 1 then 1 else 0 end) as num_goods,
sum(case when Is_Good eq 0 then 1 else 0 end) as num_bads,
calculated num_goods/&tot_good as percent_goods,
calculated num_bads/&tot_bad as percent_bads,
log(calculated percent_goods/calculated percent_bads) as woe_var,
(calculated percent_goods-calculated percent_bads) as diff_goods_bads,
calculated diff_goods_bads*calculated woe_var as IV_var
from project
group by &var._rank;
quit;
%mend woe_c;

%woe_c(Credit_Limit);
%woe_c(Income_other_Source);
/*Hence we will consider only those variables whose IV values are higher than 0.05*/

/************************Coarse Classing******************************/

data in.model;
set in.Data_Logistic;

if   Vintage="New" then Vintage_woe=-0.3119;
else Vintage_woe=0.3166;

if   Have_Car=0    then HaveCar_woe=-0.2661;
else HaveCar_woe=0.3675;

if   Have_Web_Login=1 then web_log_woe=-0.2085;
else web_log_woe=0.4522;

if      Number_Creditcard=1 then cc_woe=-0.3677;
else if Number_Creditcard=2 then cc_woe=-0.272;
else if Number_Creditcard=3 then cc_woe=-0.1066;
else if Number_Creditcard=4 then cc_woe=0.2473;

if      4790  ge Credit_Limit le 9200  then CL_woe=-1.0252;
else if 9204  ge Credit_Limit le 11599 then CL_woe=-0.6531;
else if 11600 ge Credit_Limit le 13500 then CL_woe=-0.4622;
else if 13502 ge Credit_Limit le 15200 then CL_woe=-0.3623;
else if 15204 ge Credit_Limit le 17489 then CL_woe=-0.2111;
else if 17490 ge Credit_Limit le 26900 then CL_woe=-0.0410;
else if 26910 ge Credit_Limit le 34500 then CL_woe= 0.3493;
else CL_woe=1.9944;

if              Income_Other_Source=0        then IncOtherSrce_woe=-0.3224;
else if 666  ge Income_Other_Source le 4440  then IncOtherSrce_woe=-0.2797;
else if 4444 ge Income_Other_Source le 6000  then IncOtherSrce_woe=-0.1506;
else if 6008 ge Income_Other_Source le 10974 then IncOtherSrce_woe= 0.1395;
else IncOtherSrce_woe=1.4314;

run;
/*Now we will check whether there is colinearity among the variables or not by VIF */
proc reg data = in.Model;
   model is_good = Vintage_woe
                   HaveCar_woe 
                   web_log_woe
                   cc_woe 
                   CL_woe
                   IncOtherSrce_woe / vif;
run;
/*As VIF for all the considered variables are less than 2 we will keep them-No colinearity*/
proc corr data = in.model;
   var cc_woe
       IncOtherSrce_woe;
run;

/**spliting the model into two parts 70% as training and 30% as validation**/

proc surveyselect data = in.model
                  out = in.split
                  samprate = .7 seed=1234
                  outall;
run;
 data in.training in.validation;
   set in.split;
   if selected =1 then output in.training;
   else output in.validation;
run;

/*****************************Logistic Regression******************************/

proc logistic data=in.training   descending;
model Is_Good=Vintage_woe HaveCar_woe web_log_woe cc_woe CL_woe IncOtherSrce_woe
/selection=stepwise
	ctable pprob=(0 to 1 by .1)
	slentry=0.05
	slstay=0.05
	clodds=both
	clparm=both
	lackfit ;
	output out=in.probs predicted=phat;
/* this step only for weighted logistic regression*/
	run;


/*accuracy measure KS and Rank Ordering*/
/*****************************KS&RO Calculation******************************/

proc rank data = in.probs
          out = in.logistic_probs
          group = 10;
   var phat;
   ranks r_phat;
run;

proc sql;
   create table in.KS_cal as
   select r_phat,
      sum(case when is_good eq 0 then 1 else 0 end) as bad,
      sum(case when is_good eq 1 then 1 else 0 end) as good    
    from in.logistic_probs
    group by r_phat;
quit;

data in.ks_cal;
 set in.ks_cal;
    if first.r_phat then cg = 0;
                         cg + good;
    if first.r_phat then cb = 0;
                         cb + bad;
  output;
run;

proc sql;
/*create table tt as */
select 
sum( case when Is_Good eq 1 then 1 else 0 end), 
sum(case when Is_Good eq 0 then 1 else 0 end) into :tot_good1,:tot_bad1
from in.Training;
quit;

data in.ks_cal;
 set in.ks_cal;
     percent_cg = (cg/&tot_good1.)*100;
     percent_cb = (cb/&tot_bad1.)*100;
     diff = abs(percent_cg - percent_cb);
     ro = bad/(bad+good);
run;
/*Ks is 33.93 */
/*rank ordering (bad rate) is monotonically decresing*/


/*accuracy measure Actual Vs predicted log odds */

proc sql;
   create table in.ActualVsPredicted as
   select r_phat,
      min(phat) as min_prob,
      max(phat) as max_prob,
      mean(calculated min_prob,calculated max_prob) as mean_prob,
      sum(case when is_good eq 0 then 1 else 0 end) as bad,
      sum(case when is_good eq 1 then 1 else 0 end) as good,
      log(calculated good/calculated bad) as actual,
      log(calculated mean_prob/(1 - calculated mean_prob)) as predicted
    from in.logistic_probs
    group by r_phat;
quit;

proc export data=in.ActualVsPredicted
      outfile='C:\Users\Indraj\Desktop\Sas.files\SAS Modeling (C.C.)\Results\Accuracy_ActualVsPredicted.xls'
      dbms=excel2000
      replace;
run;


/*****************************Checking Validation of The Model**********************************/

data in.validation;
   set in.validation;
   log_odds =  -1.1741 +  0.6106*CL_woe
                       +  0.5110*IncOtherSrce_woe
                       +  0.5123*HaveCar_woe  
                       -  0.5765*web_log_woe 
                       +  0.5541*Vintage_woe 
                       +  0.7699*cc_woe
                          ;
   prob = exp(log_odds)/(1+exp(log_odds));
run;

proc rank data = in.validation
          out = in.validation_rank
          group = 10;
   var prob;
   ranks r_phat;
run;

proc sql;
   create table in.KS_cal_val as
   select r_phat,
      sum(case when is_good eq 0 then 1 else 0 end) as bad,
      sum(case when is_good eq 1 then 1 else 0 end) as good    
    from in.validation_rank
    group by r_phat;
run;

data in.ks_cal_val;
            set in.ks_cal_val;
               if first.r_phat then cg = 0;
                                    cg + good;
               if first.r_phat then cb = 0;
                                    cb + bad;
               output;
         run;
proc sql;
/*create table tt as */
select 
sum( case when Is_Good eq 1 then 1 else 0 end), 
sum(case when Is_Good eq 0 then 1 else 0 end) into :tot_good2,:tot_bad2
from in.validation_rank;
quit;

data in.ks_cal_val;
 set in.ks_cal_val;
               percent_cg = (cg/&tot_good2.)*100;
               percent_cb = (cb/&tot_bad2.)*100;
               diff = abs(percent_cg - percent_cb);
               ro = bad/(bad+good);
run;
