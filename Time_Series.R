#######################################################################################
################             TIME SERIES - ANALYSIS                ####################
#######################################################################################

# Required Libraries
library("TTR")
library("tseries")
library("urca")
library("forecast")
library("normwhn.test")
library("vars")


# Import Data
births <- scan("http://robjhyndman.com/tsdldata/data/nybirths.dat")
birthstimeseries <- ts(births, frequency=12, start=c(1946,1))
birthstimeseries

# White Noise
whitenoise.test(birthstimeseries)

# Plot Time Series Data
plot.ts(birthstimeseries)


# Check of stationarity
adf.test(birthstimeseries,alternative = "stationary",k=0)
x=ur.df(birthstimeseries,type="trend",lags = 0)
summary(x)

# If the size of the seasonal fluctuations and random fluctuations
# seem to increase with the level of the time series then the model can not 
# describe by using Additive model - Thus t make it additive model we have to take 
# a transform time series data [by taking natural log]

# transform data [not required here]
birthstimeseries_L = log(birthstimeseries)

# Plot transform time series data
plot.ts(birthstimeseries_L)


# Simple Moving Average & Plot
SMA_birthstimeseries = SMA(birthstimeseries,n=12)
plot.ts(SMA_birthstimeseries)

# Decompose Time Series Data
Decompose_ts = decompose(birthstimeseries)
Decompose_ts$seasonal
Decompose_ts$trend
Decompose_ts$random
plot(Decompose_ts)

# Non Seasonal Time Series Data
NonSeasonal_ts = birthstimeseries - Decompose_ts$seasonal

####################################################################################
###########              TIME SERIES FORECASTING : HoltWinters            ##########
####################################################################################

# Fit Haltwinters
Hltwnt = HoltWinters(NonSeasonal_ts)
Hltwnt

Hltwnt$fitted

Hltwnt = HoltWinters(birthstimeseries,l.start = 23)


# Plot
plot(Hltwnt)

# Error in Prediction
Hltwnt$SSE

# Forecast Values
birthstimeseries_forecast = forecast.HoltWinters(Hltwnt,h=6)
birthstimeseries_forecast

# Plot Forecast
plot(birthstimeseries_forecast)

# Ljung-Box test
Box.test(birthstimeseries_forecast$residuals, lag=20, type="Ljung-Box")

# Plot Residuals
plot(birthstimeseries_forecast$residuals)

# ACF and PACF
acf(na.omit(birthstimeseries_forecast$residuals),lag.max = 20)

# Historgram of Forecast Error
plotForecastErrors <- function(forecasterrors)
{
  # make a histogram of the forecast errors:
  mybinsize <- IQR(forecasterrors)/4
  mysd   <- sd(forecasterrors)
  mymin  <- min(forecasterrors) - mysd*5
  mymax  <- max(forecasterrors) + mysd*3
  # generate normally distributed data with mean 0 and standard deviation mysd
  mynorm <- rnorm(10000, mean=0, sd=mysd)
  mymin2 <- min(mynorm)
  mymax2 <- max(mynorm)
  if (mymin2 < mymin) { mymin <- mymin2 }
  if (mymax2 > mymax) { mymax <- mymax2 }
  # make a red histogram of the forecast errors, with the normally distributed data overlaid:
  mybins <- seq(mymin, mymax, mybinsize)
  hist(forecasterrors, col="red", freq=FALSE, breaks=mybins)
  # freq=FALSE ensures the area under the histogram = 1
  # generate normally distributed data with mean 0 and standard deviation mysd
  myhist <- hist(mynorm, plot=FALSE, breaks=mybins)
  # plot the normal curve as a blue line on top of the histogram of forecast errors:
  points(myhist$mids, myhist$density, type="l", col="blue", lwd=2)
}
plotForecastErrors(na.omit(birthstimeseries_forecast$residuals))

####################################################################################
###########              TIME SERIES FORECASTING : ARIMA                  ##########
####################################################################################

# Checking the order of ARIMA model
a=auto.arima(NonSeasonal_ts)
a

# Run Arima Models
arima1 = arima(NonSeasonal_ts,order = c(0,1,0)) 
arima1 #aic = 315.32

arima2 = arima(NonSeasonal_ts,order = c(0,1,12))
arima2 #aic = 308.48

# Lowest AIC valued ARIMA model will be selected at last for Forecasting
arima_forecast = forecast.Arima(arima2,h=6)
arima_forecast

# Plot Forecast
plot.forecast(arima_forecast)

# Autocorrelation Check
acf(arima_forecast$residuals, lag.max=20)

# Ljung Box test for Autocorrelation
Box.test(arima_forecast$residuals, lag=20, type="Ljung-Box")

# Residual Plot
plot.ts(arima_forecast$residuals)

# make time plot of forecast errors
plotForecastErrors(arima_forecast$residuals)

###################################################################################
#########       MULTIVARIATE TIME SERIES: Vector Auto-Regressive Model      #######
###################################################################################

# Import Data
births <- scan("http://robjhyndman.com/tsdldata/data/nybirths.dat")
sales  <- log(scan("http://robjhyndman.com/tsdldata/data/fancy.dat")/100)
mydata = data.frame(cbind(births=births[1:84],sales))
mydata1 = ts(mydata,frequency=12,start=c(1946,1))

# Stationarity Check
adf.test(births,alternative = "stationary",k=0)
adf.test(sales,alternative = "stationary",k=0)

# Plot bivariate data
plot(mydata1,type = "l",col="red")

# Checking ACF
par(mfrow=c(1,2))
with(mydata,acf(births,lag.max = 50))
with(mydata,acf(sales,lag.max = 50))

# Cross correlation between two variables
par(mfrow=c(1,2))
with(mydata,ccf(births,sales, lag.max = 30, ylab = "Cross Correlation", main="Births VS Sales"))
with(mydata,ccf(ar(births)$resid, ar(sales)$resid,lag.max = 30, na.action = na.pass, ylab="Corss Correlation",main="Residual Plot of Births & Sales"))

a=with(mydata,ccf(births,sales, lag.max = 30, ylab = "Cross Correlation", main="Births VS Sales"))
a$acf

b=with(mydata,ccf(ar(births)$resid, ar(sales)$resid,lag.max = 30, na.action = na.pass, ylab="Corss Correlation",main="Residual Plot of Births & Sales"))
b$acf

# VAR model with "p=12(AR order 12 lags)
fitvar1 = VAR(mydata,p=12,type = "both",ic="AIC")
fitvar1
summary(fitvar1)


# Auto Correlation of Residuals in VAR model
acf(residuals(fitvar1)[,1])
acf(residuals(fitvar1)[,2])

# Plot of Resiudlas to check normality
plotForecastErrors(residuals(fitvar1)[,1])
plotForecastErrors(residuals(fitvar1)[,2])

# Residual Plot
plot(residuals(fitvar1)[,1],type="l")
plot(residuals(fitvar1)[,2],type="l")

# ASE MeanError
abs(sum(residuals(fitvar1)[,1]))
mean(residuals(fitvar1)[,1])
abs(sum(residuals(fitvar1)[,2]))
mean(residuals(fitvar1)[,2])


# Plot Fitted Values
plot(fitvar1)

# Plot 
predict(fitvar1)

# Serial Correlation
serial.test(fitvar1,lags.pt = 16,type = "PT.asymptotic")

###################################################################################