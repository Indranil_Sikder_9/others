
# coding: utf-8

# ### ------------------------      Optimum variable transformation for Logistic model    ---------------------------------

# #### 1. Import required packages

# In[ ]:

# Import Required packages
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from scipy.stats import zscore
from sklearn.linear_model import LogisticRegression
import gc
gc.collect()


# #### 2. Create function for all transformation & optimum variable choosing 

# In[ ]:

# Creating Variables
def Variable_all_Transformations(data,target,variable):
    data[variable]=data[variable].fillna(0)
    data['variable_maxmin'] = (data[variable] - data[variable].min())/(data[variable].max() - data[variable].min())
    data['variable^2'] = data[variable]**2
    data['variable^3'] = data[variable]**3
    data['variable^1/2'] = data[variable]**(1/2)
    data['variable^1/3'] = data[variable]**(1/3)
    data['variable^1/4'] = data[variable]**(1/4)
    data['variable^1/5'] = data[variable]**(1/5)
    data['variable_Non0'] = data[variable].replace(0,1)
    data['Log_Variable'] = np.log(data['variable_Non0'])
    data['Std_variable'] = (data[variable] - data[variable].mean())/data[variable].std()
    data['Exp_Variable'] = (np.exp(data['Std_variable']))/(1+ np.exp(data['Std_variable']))
    data['1/Variable'] = 1/data[variable]
    data['1/Variable'] = data['1/Variable'].replace(np.Inf,0)
    data['1/Variable^2'] = 1/(data[variable]*data[variable])
    data['1/Variable^2'] = data['1/Variable^2'].replace(np.Inf,0)
    data['1/Variable^3'] = 1/(data[variable]*data[variable]*data[variable])
    data['1/Variable^3'] = data['1/Variable^3'].replace(np.Inf,0)
    data['1/Variable^1/2'] = 1/(data[variable]**(1/2))
    data['1/Variable^1/2'] = data['1/Variable^1/2'].replace(np.Inf,0)
    data['1/Variable^1/3'] = 1/(data[variable]**(1/3))
    data['1/Variable^1/3'] = data['1/Variable^1/3'].replace(np.Inf,0)
   
    # Drop Variable
    data.drop(['Std_variable','variable_Non0'],inplace=True,axis=1)
 
    # Define target varible
    y = data[target]
   
    # Print
    print("*******************************************************************")
    print("Calculation of Absolute Sum of Residual for All Transformed Variables")
    print("----------------------------------------------------")
   
    # Logistic with Original Variable
    exogenous1 = [variable]
    x_1 = data[exogenous1].apply(zscore)
    logreg1=LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg1.fit(x_1,y)  
    y_pred1 = logreg1.predict_proba(x_1)[:,1]
    residual1 = y - y_pred1  
    variable_1 = round(residual1.abs().sum(),3)
    variable_1A = print(variable,"-",variable_1)
   
    # Logistic with Max_min
    exogenous2 = ['variable_maxmin']
    x_2 = data[exogenous2].apply(zscore)
    logreg2=LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg2.fit(x_2,y)  
    y_pred2 = logreg2.predict_proba(x_2)[:,1]
    residual2 = y - y_pred2  
    variable_2 = round(residual2.abs().sum(),3)
    variable_2A = print('variable_maxmin -',variable_2)
   
    # Logistic with variable^2
    exogenous3 = ['variable^2']
    x_3 = data[exogenous3].apply(zscore)
    logreg3=LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg3.fit(x_3,y)  
    y_pred3 = logreg3.predict_proba(x_3)[:,1]
    residual3 = y - y_pred3  
    variable_3 = round(residual3.abs().sum(),3)
    variable_3A = print('variable^2 -',variable_3)
   
    # Logistic with variable^3
    exogenous4 = ['variable^3']
    x_4 = data[exogenous4].apply(zscore)
    logreg4=LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg4.fit(x_4,y)  
    y_pred4 = logreg4.predict_proba(x_4)[:,1]
    residual4 = y - y_pred4  
    variable_4 = round(residual4.abs().sum(),3)
    variable_4A = print('variable^3 -',variable_4)
   
    # Logistic with variable^1/2
    exogenous5 = ['variable^1/2']
    x_5 = data[exogenous5].apply(zscore)
    logreg5 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg5.fit(x_5,y)  
    y_pred5 = logreg5.predict_proba(x_5)[:,1]
    residual5 = y - y_pred5  
    variable_5 = round(residual5.abs().sum(),3)
    variable_5A = print('variable^1/2 -',variable_5)
   
    # Logistic with variable^1/2
    exogenous6 = ['variable^1/3']
    x_6 = data[exogenous6].apply(zscore)
    logreg6 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg6.fit(x_6,y)  
    y_pred6 = logreg6.predict_proba(x_6)[:,1]
    residual6 = y - y_pred6  
    variable_6 = round(residual6.abs().sum(),3)
    variable_6A = print('variable^1/3 -',variable_6)
   
    # Logistic with variable^1/4
    exogenous7 = ['variable^1/4']
    x_7 = data[exogenous7].apply(zscore)
    logreg7 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg7.fit(x_7,y)  
    y_pred7 = logreg7.predict_proba(x_7)[:,1]
    residual7 = y - y_pred7  
    variable_7 = round(residual7.abs().sum(),3)
    variable_7A = print('variable^1/4 -',variable_7)
   
    # Logistic with variable^1/5
    exogenous8 = ['variable^1/5']
    x_8 = data[exogenous8].apply(zscore)
    logreg8 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg8.fit(x_8,y)  
    y_pred8 = logreg8.predict_proba(x_8)[:,1]
    residual8 = y - y_pred8  
    variable_8 = round(residual8.abs().sum(),3)
    variable_8A = print('variable^1/5 -',variable_8)
 
    # Logistic with Log_Variable
    exogenous9 = ['Log_Variable']
    x_9 = data[exogenous9].apply(zscore)
    logreg9 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg9.fit(x_9,y)  
    y_pred9 = logreg9.predict_proba(x_9)[:,1]
    residual9 = y - y_pred9  
    variable_9 = round(residual9.abs().sum(),3)
    variable_9A = print('Log_Variable -',variable_9)
   
    # Logistic with '1/Variable'
    exogenous10 = ['1/Variable']
    x_10 = data[exogenous10].apply(zscore)
    logreg10 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg10.fit(x_10,y)  
    y_pred10 = logreg10.predict_proba(x_10)[:,1]
    residual10 = y - y_pred10  
    variable_10 = round(residual10.abs().sum(),3)
    variable_10A = print('1/Variable -',variable_10)
    
    # Logistic with '1/Variable^2'
    exogenous11 = ['1/Variable^2']
    x_11 = data[exogenous11].apply(zscore)
    logreg11 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg11.fit(x_11,y)  
    y_pred11 = logreg11.predict_proba(x_11)[:,1]
    residual11 = y - y_pred11  
    variable_11 = round(residual11.abs().sum(),3)
    variable_11A = print('1/Variable^2 -',variable_11)
   
    # Logistic with '1/Variable^3'
    exogenous12 = ['1/Variable^3']
    x_12 = data[exogenous12].apply(zscore)
    logreg12 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg12.fit(x_12,y)  
    y_pred12 = logreg12.predict_proba(x_12)[:,1]
    residual12 = y - y_pred12  
    variable_12 = round(residual12.abs().sum(),3)
    variable_12A = print('1/Variable^3 -',variable_12)
   
    # Logistic with '1/Variable^1/2'
    exogenous13 = ['1/Variable^1/2']
    x_13 = data[exogenous13].apply(zscore)
    logreg13 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg13.fit(x_13,y)  
    y_pred13 = logreg13.predict_proba(x_13)[:,1]
    residual13 = y - y_pred13  
    variable_13 = round(residual13.abs().sum(),3)
    variable_13A = print('1/Variable^1/2 -',variable_13)
   
    # Logistic with '1/Variable^1/3'
    exogenous14 = ['1/Variable^1/3']
    x_14 = data[exogenous14].apply(zscore)
    logreg14 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg14.fit(x_14,y)  
    y_pred14 = logreg14.predict_proba(x_14)[:,1]
    residual14 = y - y_pred14  
    variable_14 = round(residual14.abs().sum(),3)
    variable_14A = print('1/Variable^1/3 -',variable_14)
   
    # Logistic with Exp_Variable
    exogenous15 = ['Exp_Variable']
    x_15 = data[exogenous15].apply(zscore)
    logreg15 = LogisticRegression(C=1,max_iter=100,penalty='l2',solver='saga',multi_class='ovr',class_weight="balanced",random_state=9)
    logreg15.fit(x_15,y)  
    y_pred15 = logreg15.predict_proba(x_15)[:,1]
    residual15 = y - y_pred15  
    variable_15 = round(residual14.abs().sum(),3)
    variable_15A = print('Exp_Variable -',variable_15)
   
    # drop all transformed variables
    data.drop(['variable_maxmin','variable^2','variable^3','variable^1/2','variable^1/3','variable^1/4','variable^1/5',
               'Log_Variable','Exp_Variable','1/Variable','1/Variable^2','1/Variable^3','1/Variable^1/2','1/Variable^1/3'], inplace=True, axis=1)
   
    # Minimum Sum of Absolute residual
    print("----------------------------------------------------")
    min_value = min(variable_1,variable_2,variable_3,variable_4,variable_5,variable_6,variable_7,variable_8,variable_9,variable_10,variable_11,variable_12,variable_13,variable_14,variable_15)
 
    ## Take the best transformation by minimising residul of logistc 
    if min_value == variable_1:
        print("Best:- Original Variable, (RSS=",variable_1,")",sep="")
    elif min_value == variable_2:
        print("Best:- Max min Transformation, (RSS=",variable_2,")",sep="")
    elif min_value == variable_3:
        print("Best:- Square Transformation, (RSS=",variable_3,")",sep="")
    elif min_value == variable_4:
        print("Best:- Cube Transformation, (RSS=",variable_4,")",sep="")
    elif min_value == variable_5:
        print("Best:- Square root Transformation, (RSS=",variable_5,")",sep="")
    elif min_value == variable_6:
        print("Best:- Power 1/3 Transformation, (RSS=",variable_6,")",sep="")
    elif min_value == variable_7:
        print("Best:- Power 1/4 Transformation, (RSS=",variable_7,")",sep="")
    elif min_value == variable_8:
        print("Best:- Power 1/5 Transformation, (RSS=",variable_8,")",sep="")
    elif min_value == variable_9:
        print("Best:- Log Transformation, (RSS=",variable_9,")",sep="")
    elif min_value == variable_10:
        print("Best:- 1 Divided Transformation, (RSS=",variable_10,")",sep="")
    elif min_value == variable_11:
        print("Best:- 1 Devided square Transformation, (RSS=",variable_11,")",sep="")
    elif min_value == variable_12:
        print("Best:- 1 Devided Cube Transformation, (RSS=",variable_12,")",sep="")
    elif min_value == variable_13:
        print("Best:- 1 Devided Square Root Transformation, (RSS=",variable_13,")",sep="")
    elif min_value == variable_14:
        print("Best:- 1 Devided 1/3 Powered Transformation, (RSS=",variable_14,")",sep="")
    else:
        print("Best:- Exponential Transformation (RSS=",variable_15,")",sep="")
       
    Difference_RSS = variable_1 - min_value
    print("Difference in RSS with Original Variable:",round(Difference_RSS,3))
    print("*******************************************************************")


# #### 3. Output of Variable_all_Transformations() function

# In[ ]:

## Import data
mydata = pd.read_csv(r"C:\Users\DEMO\Desktop\Jai Sri Krishna\Python\Data/masterdata1.csv")
mydata.shape


# In[ ]:

## Test of function
Variable_all_Transformations(mydata,'Cust_char','dti')


# In[ ]:

## Result - printed
*******************************************************************
Calculation of Absolute Sum of Residual for All Transformed Variables
----------------------------------------------------
dti - 1330.248
variable_maxmin - 1330.248
variable^2 - 1328.432
variable^3 - 1326.673
variable^1/2 - 1330.832
variable^1/3 - 1330.927
variable^1/4 - 1330.953
variable^1/5 - 1330.959
Log_Variable - 1330.981
1/Variable - 1330.315
1/Variable^2 - 1330.651
1/Variable^3 - 1330.773
1/Variable^1/2 - 1330.323
1/Variable^1/3 - 1330.473
Exp_Variable - 1330.416
----------------------------------------------------
Best:- Cube Transformation, (RSS=1326.673)
Difference in RSS with Original Variable: 3.575
*******************************************************************


# #### -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
